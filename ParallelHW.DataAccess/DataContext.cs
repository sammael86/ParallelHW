﻿using Microsoft.EntityFrameworkCore;
using ParallelHW.Core.Entities;

namespace ParallelHW.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\MSSQLLocalDB;Database=parallelloader;Integrated Security=true;");
        }
    }
}