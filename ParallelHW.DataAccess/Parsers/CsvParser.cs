﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using CsvHelper;
using ParallelHW.Core.Parsers;
using ParallelHW.Core.Entities;

namespace ParallelHW.DataAccess.Parsers
{
    public class CsvParser : IDataParser<List<Customer>>
    {
        private readonly string _dataFilePath;

        public CsvParser(string dataFilePath)
        {
            _dataFilePath = $"{dataFilePath}.csv";
        }

        public List<Customer> Parse()
        {
            Console.WriteLine("Parsing CSV file");
            using var reader = new StreamReader(_dataFilePath);
            using var csv = new CsvReader(reader, CultureInfo.InvariantCulture);
            return csv.GetRecords<Customer>().ToList();
        }
    }
}