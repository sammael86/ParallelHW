using ParallelHW.Core.Entities;
using ParallelHW.Core.Repositories;

namespace ParallelHW.DataAccess.Repositories
{
    public class CustomerRepository
        : ICustomerRepository
    {
        private readonly DataContext _db;

        public CustomerRepository()
        {
            _db = new DataContext();
        }

        public void AddCustomer(Customer customer)
        {
            _db.Customers.Add(new Customer
            {
                FullName = customer.FullName,
                Email = customer.Email,
                Phone = customer.Phone
            });
        }

        public void Save()
        {
            _db.SaveChanges();
        }
    }
}