using System;
using System.Threading;
using ParallelHW.Core.Loaders;

namespace ParallelHW.Loader.Loaders
{
    public class FakeDataLoader
        : IDataLoader
    {
        public void LoadData()
        {
            Console.WriteLine("Loading data...");
            Thread.Sleep(10000);
            Console.WriteLine("Loaded data...");
        }
    }
}