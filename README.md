## Общая структура проекта для домашнего задания по параллельной загрузке данных

### ParallelHW.Loader

Консольное приложение, которое должно сгенирировать файл с данными и запустить загрузку из него через реализацию `IDataLoader`.

### ParallelHW.DataGenerator

Библиотека, в которой должна определена логика генерации файла с данными, в базовом варианте это XML.

### ParallelHW.DataGenerator.App

Консольное приложение, которое позволяет при запуске отдельно выполнить генерацию файла из `DataGenerator` библиотеки.

### ParallelHW.DataAccess

Библиотека, в которой находится доступ к базе данных и файлу с данными.

### ParallelHW.Core

Библиотека, в которой определены сущности БД и основные интерфейсы, которые реализуют другие компоненты.
